//
//  ViewController.swift
//  FirebaseDemoApp
//
//  Created by user199248 on 5/26/21.
//

import UIKit
import Firebase
import FirebaseCrashlytics
import AVKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var logoIV: UIImageView!
    
    @IBOutlet weak var displayIV: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        Analytics.setAnalyticsCollectionEnabled(true)
        
        Analytics.setUserID("987654321")
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
          AnalyticsParameterItemID: "id-demo",
          AnalyticsParameterItemName: "demo",
          AnalyticsParameterContentType: "cont"
          ])
        //setScreenName("blog_view_controller", screenClass: classForCoder.description())
        Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(true)
        
        Crashlytics.crashlytics().log("View loaded")

        Crashlytics.crashlytics().setCustomValue(42, forKey: "MeaningOfLife")
        Crashlytics.crashlytics().setCustomValue("Test value", forKey: "last_UI_action")
        Crashlytics.crashlytics().setUserID("987654321")
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0]

            //make a file name to write the data to using the documents directory
            let fileName = "\(documentsDirectory)/perfsamplelog.txt"

            // Start tracing
            let trace = Performance.startTrace(name: "request_trace")

            let contents: String
            do {
              contents = try String(contentsOfFile: fileName, encoding: .utf8)
            } catch {
              print("Log file doesn't exist yet")
              contents = ""
            }

            let fileLength = contents.lengthOfBytes(using: .utf8)

            trace?.incrementMetric("log_file_size", by: Int64(fileLength))

            let target = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
            guard let targetUrl = URL(string: target) else { return }
            guard let metric = HTTPMetric(url: targetUrl, httpMethod: .get) else { return }
            metric.start()

            var request = URLRequest(url:targetUrl)
            request.httpMethod = "GET"

            let task = URLSession.shared.dataTask(with: request) {
              data, response, error in

              if let httpResponse = response as? HTTPURLResponse {
                metric.responseCode = httpResponse.statusCode
              }
              metric.stop()

              if let error = error {
                print("error=\(error)")
                return
              }


              DispatchQueue.main.async {
                self.logoIV.image = UIImage(data: data!)
              }

              //trace?.stop()

              if let absoluteString = response?.url?.absoluteString {
                let contentToWrite = contents + "\n" + absoluteString
                do {
                  try contentToWrite.write(toFile: fileName, atomically: false, encoding: .utf8)
                } catch {
                  print("Can't write to log file")
                }
              }
            }

            task.resume()
            trace?.incrementMetric("request_sent", by: 1)
            
            if #available(iOS 10, *){
              let asset =
                AVURLAsset(url: URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Two_red_dice_01.svg/220px-Two_red_dice_01.svg.png")!)
              let downloadSession =
                AVAssetDownloadURLSession(configuration: URLSessionConfiguration.background(withIdentifier: "avasset"),
                                          assetDownloadDelegate: nil,
                                          delegateQueue: OperationQueue.main)
                
              let task = downloadSession.makeAssetDownloadTask(asset: asset,
                                                               assetTitle:"something",
                                                               assetArtworkData:nil,
                                                               options:nil)!
              task.resume()
              trace?.incrementMetric("av_request_sent", by: 1)
                
                trace?.stop()
            }
    }


    @IBAction func cricketButton(_ sender: UIButton) {
        
        let url = URL(string: "https://northernspinal.com.au/wp-content/uploads/2018/01/cricket-injuries-1200x565.jpg")!

                    // Fetch Image Data
                    if let data = try? Data(contentsOf: url) {
                        // Create Image and Update Image View
                        displayIV.image = UIImage(data: data)
                    }
    }
    
    
    @IBAction func footballButton(_ sender: UIButton) {
        
        let url = URL(string: "https://cdn.britannica.com/s:800x1000/51/190751-050-147B93F7/soccer-ball-goal.jpg")!

                    // Fetch Image Data
                    if let data = try? Data(contentsOf: url) {
                        // Create Image and Update Image View
                        displayIV.image = UIImage(data: data)
                    }
    }
    
    
    @IBAction func motoGPButton(_ sender: UIButton) {
        
        let url = URL(string: "https://www.iamabiker.com/wp-content/uploads/2019/10/Joan-Mir-Valentino-Rossi-MotoGP-HD-wallpaper-Buriram.jpg")!

                    // Fetch Image Data
                    if let data = try? Data(contentsOf: url) {
                        // Create Image and Update Image View
                        displayIV.image = UIImage(data: data)
                    }
    }
    
    @IBAction func nonFatalButton(_ sender: UIButton) {
        let userInfo = [
                  NSLocalizedDescriptionKey: NSLocalizedString("The request failed.", comment: ""),
                  NSLocalizedFailureReasonErrorKey: NSLocalizedString("The response returned a 404.", comment: ""),
                  NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString("Does this page exist?", comment:""),
                      "ProductID": "123456"
                    ]
                let error = NSError(domain: NSURLErrorDomain, code: -1001, userInfo: userInfo)
                Crashlytics.crashlytics().record(error: error)
        
    }
    
    
    @IBAction func fatalButton(_ sender: UIButton) {
        Crashlytics.crashlytics().log("Cause Crash button clicked")
                //fatalError()
        NSException(name:NSExceptionName(rawValue: "name"), reason:"reason", userInfo:nil).raise()
        
        
        
    }
}

